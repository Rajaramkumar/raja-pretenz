import express from 'express';
import 'express-async-errors';
import { json } from 'body-parser';
import cookieSession from 'cookie-session';

const app = express();
app.set('trust proxy', true);
app.use(json());

app.use(cookieSession({
    secure: true,
    signed: false
}));

app.get('/', (req, res) => {
    res.send("hi");
})

export {app};